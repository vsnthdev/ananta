//   ___    __________   |  Vasanth Developer (Vasanth Srivatsa)
//   __ |  / /___  __ \  |  ------------------------------------------------
//   __ | / / __  / / /  |  https://github.com/vasanthdeveloper/ananta.git
//   __ |/ /  _  /_/ /   |
//   _____/   /_____/    |  The entryfile for ananta project
//                       |

import { ConfigImpl, LocalStorageImpl } from './interfaces'
import { tick, fetchData, resetCounter } from './data_handler'
import event from './events'
import defaultConfig from './config'

let currentConfig: ConfigImpl = null
const localStorage: LocalStorageImpl = {
    loadCount: 0,
}

// scrolling() is the hook function which will be hooked to onscroll event
function scrolling(): void {
    tick(currentConfig, window.scrollY, localStorage)
}

// init() will take the config and hook things
function init(config): void {
    // Check if a config was provided
    if (config) {
        // Loop through all possible config keys, fill the defaults to keys
        // for which the user hasn't specified any value
        for (const obj in defaultConfig) {
            if (config[obj] === undefined) {
                config[obj] = defaultConfig[obj]
            }
        }

        // Validate the config object to check if all the required values are present
        if (config.uri == undefined || config.uri == '') {
            console.error('ananta: The uri config value was not provided.')
            return
        }

        // Make the config global
        localStorage.loadCount = config.initCount
        currentConfig = config

        // Connect the onscroll event
        if (currentConfig.automatic == true) {
            window.onscroll = scrolling
            tick(currentConfig, window.scrollY, localStorage)
        }

        // Tell the user that this module got initialized
        if (currentConfig.quiet == false) {
            console.info('ananta – Developed & Maintained By Vasanth Developer.')
            console.info(
                `Version ${require('../package.json').version}. Checkout: https://github.com/vasanthdeveloper/ananta`,
            )
        }

        // emit the init event
        event.emit('init')
    } else {
        console.log('ananta: No configuration passed.')
        return
    }
}

// getConfig() will return the currentConfig object
function getConfig(): ConfigImpl {
    return currentConfig
}

// fetch() will fetch new pages manually
function fetch(): void {
    resetCounter()
    if (currentConfig.automatic == true) {
        window.onscroll = scrolling
    }
    fetchData(currentConfig, window.scrollY, localStorage)
}

// reset() will reset the plugin to it's initial state
// useful for simple page applications
function reset(): void {
    localStorage.loadCount = currentConfig.initCount
    resetCounter()
}

// Attach our local functions into window object
// So, it can be used publicly
window['ananta'] = {
    init,
    fetch,
    getConfig,
    event,
    localStorage,
    reset,
}
