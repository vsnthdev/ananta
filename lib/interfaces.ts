// The interface skeleton for a configuration
export interface ConfigImpl {
    captureSelector: string
    container: string
    buffer: number
    automatic: boolean
    quiet: boolean
    limit: number
    initCount: number
    uri?: string
}

// The interface skeleton for a local storage object
export interface LocalStorageImpl {
    loadCount: number
}
