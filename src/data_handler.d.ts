import { ConfigImpl, LocalStorageImpl } from './interfaces';
export declare function fetchData(currentConfig: ConfigImpl, lastScrollY: number, localStorage: LocalStorageImpl): void;
export declare function tick(currentConfig: ConfigImpl, lastScrollY: number, localStorage: LocalStorageImpl): void;
export declare function resetCounter(): void;
//# sourceMappingURL=data_handler.d.ts.map