/*eslint-env node */

const path = require('path')

const browserSync = require('browser-sync-webpack-plugin')

module.exports = {
    entry: path.join(__dirname, 'lib', 'ananta.ts'),
    mode: 'production',
    output: {
        filename: 'ananta.js',
        path: path.join(__dirname, 'src'),
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    plugins: [
        new browserSync({
            // browse to http://localhost:8080/ during development
            host: '0.0.0.0',
            port: 8080,
            server: { baseDir: __dirname },
            files: `${__dirname}/**/*`,
            open: false,
            notify: false,
        }),
    ],
}
